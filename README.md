
## MLServer
---

Aplicação desenvolvida para o componente referente a Model Serving do [MBA de Machine Learning in Production da UFScar](https://iti.ufscar.mba/mlp).

O objetivo da tarefa era configurar o [MLServer](https://mlserver.readthedocs.io/en/latest/) para hospedar um modelo de sua preferência, utilizando um dos motores de inferência nativos ou construir um próprio.

Optei pelo desenvolvimento de dois modelos para o problema de previsão de resistência do concreto à compressão cuja base de dados encontra-se no [UCI](https://archive-beta.ics.uci.edu/ml/datasets/concrete+compressive+strength). Este conjunto de dados apresenta as seguintes variáveis:

* Cimento (kg por m3)
* Escória de Alto Forno (kg por m3)
* Cinza volante (kg por m3)
* Água (kg por m3)
* Superplastificante (kg por m3)
* Agregado grosso (kg por m3)
* Agregado Fino (kg por m3)
* Idade em dias (1~365)
* Resistência à compressão do concreto

Para subir o servidor basta executar o seguindo comando:
~~~bash
docker-compose up --build
~~~

Com o container em execução pode-se verificar o status das versões de ambos os modelos com: 

* GET localhost:8080/v2/models/concrete-compression-predict/versions/1.0/ready
* GET localhost:8080/v2/models/concrete-compression-predict/versions/2.0/ready

Para verificar a inferência utiliza-se as seguintes rotas:
* POST localhost:8080/v2/models/concrete-compression-predict/versions/1.0/infer
* POST localhost:8080/v2/models/concrete-compression-predict/versions/2.0/infer

Alguns exemplos de body a serem enviados nas requisições para as duas rotas acima:

~~~json
{
    "inputs":[
        {
            "name": "teste",
            "shape":[1,8],
            "datatype":"FP64",
            "data": [[ 540, 0, 	0, 	162, 2.5, 1040, 676, 28]]
        }
    ]
}
~~~

~~~json
{
    "inputs":[
        {
            "name": "teste",
            "shape":[2,8],
            "datatype":"FP64",
            "data": [
                [ 237.5, 237.5, 	0, 	228, 	0, 	932, 	594, 	365 ],
                [380, 0, 	0, 	228, 	0, 	932, 	670,	180 ]
            ]
        }
    ]
}
~~~